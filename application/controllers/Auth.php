<?php
	class Auth extends CI_Controller
	{

		function __construct()
		{
			parent::__construct();
		}
		public function index()
		{
			$data['judul'] = "Sign In";
			$this->load->view("signin/view.php",$data);
		}
		public function proses()
		{
			$user = $this->input->post('user');
			$pass = $this->input->post('pass');
			$sql = $this->db->get_where('tb_user',['username'=>$user]);
			$row = $sql->row();
			$count = $sql->num_rows();
			if ($count == 0) {
				?>
					<script type="text/javascript">
						alert("Anda gagal login!!");
						window.location='index';
					</script>
				<?php
			}else{
				if (password_verify($pass,$row->password)) {
					$a = [
						'id_user' => $row->id_user,
						'nama' => $row->nama_depan.' '.$row->nama_belakang,
						'user' => $row->username,
						'level' => $row->level
					];
					$this->session->set_userdata($a);
					$this->session->set_flashdata('success','Selamat Datang '.$row->username);
					redirect(site_url('beranda/p/view'));
				}else{
					?>
						<script type="text/javascript">
							alert("Anda gagal login!!");
							window.location='index';
						</script>
					<?php
				}
			}
		}
		public function signout()
		{
			$a = ['id_user','user','level'];
			$this->session->unset_userdata($a);
			$this->session->set_flashdata('success','Sampai Jumpa');
			redirect(site_url('auth'));
		}
	}
 ?>

<?php
	class Pengelola extends CI_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this->load->model('ModelPengelola');
		}
		public function p()
		{
			$p = $this->uri->segment(3);
			$data['judul'] = "Data pengelola";
			$data['folder'] = "pengelola";

			$data['p'] = $p;
			if ($p == "view") {
				$data['val'] = $this->ModelPengelola->views("tb_user")->result();
				$this->load->view('index',$data);
			}elseif ($p == "input") {
				$id = $this->uri->segment(4);
				if (empty($id)) {
					$data['judul'] = "Inpu data pengelola";
					$data['btn'] = "Simpan";
					$data['url'] = "pengelola/simpan";

					$this->load->view('index', $data);
				}else{
					$data['judul'] = "Edit data pengelola";
					$data['btn'] = "Edit";
					$data['url'] = "pengelola/edit";
					$data['val'] = $this->ModelPengelola->all("tb_user","WHERE id_user = '$id'");
					$this->load->view("index",$data);
				}
			}elseif($p == 'forgot'){
				$id = $this->uri->segment(4);
				$data['id'] = $id;
				$row = $this->db->get_where('tb_user',['id_user'=>$id])->row();
				$data['row'] = $row;
				$data['judul'] = "Reset Password - ".$row->nama_depan.' '.$row->nama_belakang;
				$data['url'] = "pengelola/forgot";

				$this->load->view('index', $data);
			}
		}
		public function simpan()
		{
			$val = array(
                'nama_depan' => $this->input->post('nama_depan'),
				'nama_belakang' => $this->input->post('nama_belakang'),
				'username' => $this->input->post('username'),
				'password' => password_hash($this->input->post('password'),PASSWORD_DEFAULT),
				'level' => $this->input->post('level')
			);
			$this->ModelPengelola->simpan($val);
			$this->session->set_flashdata('success','Data berhasil di simpan');
			redirect('pengelola/p/view');
		}
		public function edit()
		{
			$id = $this->input->post('id_user');
			$val = array(
				'nama_depan' => $this->input->post('nama_depan'),
				'nama_belakang' => $this->input->post('nama_belakang'),
				'username' => $this->input->post('username'),
				'level' => $this->input->post('level')
			);
			$this->ModelPengelola->edit($id,$val);
			$this->session->set_flashdata('success','Data berhasil di perbaharui');
			redirect('pengelola/p/view');
		}
		public function forgot()
		{
			$id = $this->input->post('id_user');
			if ($this->input->post('password') != $this->input->post('retype')) {
				$this->session->set_flashdata('err','Password gagal diubah');
				redirect('pengelola/p/view');
			}else{
				$val = array(
					'password' => password_hash($this->input->post('password'),PASSWORD_DEFAULT)
				);
				$this->ModelPengelola->edit($id,$val);
				$this->session->set_flashdata('success','Password berhasil diubah');
				redirect('pengelola/p/view');
			}
		}
		public function hapus($id)
		{
			$this->ModelPengelola->hapus($id);
			$this->session->set_flashdata('success','Data berhasil di hapus');
			redirect('pengelola/p/view');
		}
	}
 ?>

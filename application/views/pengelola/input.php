<?php
	if ($btn == "Edit") {
		$data = $val->row_array();
		$pass = 'hidden';
	}else{
		$data['nama_depan'] = "";
		$data['nama_belakang'] = "";
		$data['username'] = "";
		$data['otoritas'] = "";
		$pass = '';
	}
 ?>

<div class="panel box-shadow-none content-header">
	<div class="panel-body capitalize">
		<div class="col-md-12">
			<h3 class="animated fadeInLeft"><?= $judul ?></h3>
			<p class="animated fadeInDown capitalize">
				<?= $folder?> <span class="fa-angle-right fa"></span> <?= $p ?> <span class="fa-angle-right fa"></span>
				<?= $btn ?>
			</p>
		</div>
	</div>
</div>

<div class="form-element">
	<div class="col-md-12 padding-0">
		<div class="col-md-12">
			<div class="panel form-element-padding">
				<div class="panel-body">
					<div class="col-md-12">
						<?= form_open_multipart($url); ?>
						<input type="hidden" name="id_user" value="<?php if (isset($data['id_user'])) { echo $data['id_user'];} ?>">
						<div>
							<div class="form-group">
								<label class="col-sm-2 control-label text-right">Nama Depan</label>
								<div class="col-sm-10">
									<input type="text" name="nama_depan" value="<?= $data['nama_depan'];?>" class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label text-right">Nama Belakang</label>
								<div class="col-sm-10">
									<input type="text" name="nama_belakang" value="<?= $data['nama_belakang'];?>" class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label text-right">Username</label>
								<div class="col-sm-10">
									<input type="text" name="username" value="<?= $data['username'];?>" class="form-control" required>
								</div>
							</div>
							<div class="form-group" <?= $pass; ?> >
								<label class="col-sm-2 control-label text-right">Password</label>
								<div class="col-sm-10">
									<input type="password" name="password" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label text-right">Otoritas</label>
								<div class="col-sm-10">
									<select name="level" class="form-control padding-0 select" required>
										<option value="">--Pilih--</option>
										<option <?php if (isset($data['level']) == 'petugas') {echo 'selected';} ?> value="petugas">Petugas</option>
										<option <?php if (isset($data['level']) == 'admin') {echo 'selected';}?> value="admin">Administrator</option>
										<option <?php if (isset($data['level']) == 'manager') {echo 'selected';}?> value="manager">Manajer</option>
										<option <?php if (isset($data['level']) == 'superuser') {echo 'selected';}?> value="superuser">Super User</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<input type="submit" value="<?= $btn; ?>" class="btn btn-raised btn-primary">
								<input type="reset" value="Reset" class="btn btn-raised btn-warning">
							</div>
						</div>
						<?= form_close();?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
